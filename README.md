# Documentatie FCP

## API

| Sectiune | Descriere |
| ---------- | --------- |
| [Prezentare](api/)  | Prezentare generala. Workflow, variabile comune, etc. |
| [Changelog](CHANGELOG.md)  | Changelog api. |
