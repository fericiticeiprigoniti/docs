# Changelog

### 1.10.2020
- adaugat endpoint /biblioteca/autori (# GET ./biblioteca/autori)
- adaugat endpoint /biblioteca/carti (# GET ./biblioteca/carti)
- adaugat endpoint /poezii/list (# GET ./poezii/list)
- start documentatie
