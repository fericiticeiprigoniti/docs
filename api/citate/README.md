# Sumar
| Metoda | Endpoint     | Sumar                         |
| ------ | ---------    | ----------------------------- |
| GET    | [./citate/random](#get-citaterandom)     | Returneaza un citat random |


# GET ./citate/random
Lista cu toate poeziile filtrare dupa anumite criterii
exemplu:
http://fcp.cpco.ro/api/citate/random?personajId=170

[Output example](./get-citaterandom-example.json) 

[Demo](http://fcp.cpco.ro/api/citate/random)

### GET params
| Nume          | Tip       | Req.  | Validari generice  |  Detalii  |
| ------------- |---------- | ----- | ------------------ | --------- |
| personajId    | int       | n     | numeric | ID-ul personajului |
| carteId    | int       | n     | numeric | ID-ul cartii |
| vizibilPi    | int       | n     | numeric | 1- daca este vizibil citatul pe proiectul poetiiinchisorilor |
| locPatimireId    | int       | n     | numeric | ID loc patimire |
| orderBy    | array       | n     |  | id_asc,id_desc, order_rand (default e order_rand) |
