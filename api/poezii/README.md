# Sumar
| Metoda | Endpoint     | Sumar                         |
| ------ | ---------    | ----------------------------- |
| GET    | [./poezii/list](#get-poeziilist)     | Lista cu toate poeziile |
| GET    | [./poezii/elemente_versificatie](#get-poeziielemente_versificatie)     | Lista cu toate elementele de versificatie (rima/ciclu/structura etc |
| GET    | [./poezii/perioade_creatie](#get-poeziiperioade_creatie)     | Lista cu perioade creatie |


# GET ./poezii/list
Lista cu toate poeziile filtrare dupa anumite criterii
exemplu:
http://fcp.cpco.ro/api/poezii/poezii?personajId=1&titleLike=faur

Pentru a afișa toate variantele unei poezii se foloseste parametrul parentId:
http://fcp.cpco.ro/api/poezii/poezii?parentId=1528

Pentru afisarea random (shuffle) setam orderBy[] cu parametrul 'order_rand'

[Output example](./get-poeziilist-example.json) 

[Demo](http://fcp.cpco.ro/api/poezii/list)

### GET params
| Nume          | Tip       | Req.  | Validari generice  |  Detalii  |
| ------------- |---------- | ----- | ------------------ | --------- |
| id    | int       | n     | numeric | ID-ul poeziei |
| excludedId    | int       | n     |  | ID-ul poeziei care vrem sa fie exclusa din lista |
| titleLike    | string       | n     |  | Cauta in titlul poeziei 
| parentId    | int       | n     |  | ID-ul poeziei principale | 
| personajId    | int       | n     |  | ID-ul personajului |
| contentLike    | string       | n     |  | Cauta in continut |
| sursaDocCarteId    | int       | n     |  | ID-ul cartii din biblioteca |
| sursaDocPublicatieId    | int       | n     |  | ID-ul cartii/publicatiei din biblioteca |
| perioadaCreatieiId    | int       | n     |  | ID-ul perioada creatie |
| subjectId    | int       | n     |  | ID-ul subiectului |
| orderBy    | array       | n     |  | id_asc,id_desc, order_id_asc, order_id_desc, order_rand |


Alti parametri posibili pentru a fi folositi:

- @var int $comentariuArticolId
- @var int $comentariuUserId
- @var int $subjectId
- @var int $perioadaCreatieiId
- @var string $creationPeriodLike
- @var string $creationPlaceLike
- @var int $specieId
- @var int $structuraStrofaId
- @var int $rimaId
- @var int $piciorMetricId
- @var int $parentId
- @var int $userId
- @var array $tagIdList


# GET ./poezii/elemente_versificatie
Lista cu toate elementele de versificatie (nomenclator)

[Output example](./get-poeziielemente_versificatie-example.json) 

[Demo](http://fcp.cpco.ro/api/poezii/elemente_versificatie)


# GET ./poezii/perioade_creatie
Lista cu toate perioadele de creatie

[Output example](./get-poeziiperioade_creatie-example.json) 

[Demo](http://fcp.cpco.ro/api/poezii/perioade_creatie)

### GET params
| Nume          | Tip       | Req.  | Validari generice  |  Detalii  |
| ------------- |---------- | ----- | ------------------ | --------- |
| id    | int       | n     | numeric | ID-ul perioadei |
| excludedId    | int       | n     |  | ID-ul perioadei care vrem sa fie exclusa din lista |