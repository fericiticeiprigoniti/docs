# Sumar
| Sectiune  | Descriere     |
| --------- | --------- |
| [Generalitati](#generalitati)                                     | Informatii generale.  | 
| [HTTP status codes](#http-status-codes)                           | Coduri HTTP folosite.   |

# Generalitati
- toate call-urile au ca url de baza http://fcp.cpco.ro/api/
- toate raspunsurile valide vor fi in format JSON cu exceptia endpoint-urilor unde e specificat altfel

# HTTP status codes
- se considera ca request-ul a reusit doar daca status code-ul este 200 indiferent de continutul din body.
- e datoria programatorului sa verifice prima data status code-ul si apoi sa citeasca bodyul


| Cod           | Info          |     Detalii  |
| ------------- |-------------- | --- |
| 200           | OK.           | Request-ul a reusit. |
| 400           | Bad request   | Requestul nu are un format corespunzator sau nu a trecut de validare. Informatii despre erori vor fi disponibile in body |
| 401           | Unauthorized  | Endpoint-ul necesita autentificare. |
| 403           | Forbidden     | Utilizatorul nu are permisiunile necesare pentru a executa request-ul. |
| 404           | Not found     | Duh. |
| 405           | Not supported     | Parametrii invalizi. Pot fi folosiți doar cei din documentație |
| 420           | Deprecated    | Versiunea apelata nu mai este suportata. |
| 429           | Too Many Requests    | Erroare temporara in cazul in care se depaseste limita de request-uri / ip |
| 503           | Service Unavailable       | Serviciul este in mentenanta. Eroare temporara. |
| 50x           | Internal Server Error     | Eroare interna. |
