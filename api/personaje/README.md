IN PROGRESS

# Sumar
| Metoda | Endpoint     | Sumar                         |
| ------ | ---------    | ----------------------------- |
| GET    | [./personaje/personaj](#get-zoomuser)     | Verifica detaliile unui utilizator si statusul |


# GET ./personaje/personaj
Lista cu toate personajele filtrare dupa anumite criterii

[Link exemplu](http://fcp.cpco.ro/api/personaje/personaj)  


### GET params
| Nume          | Tip       | Req.  | Validari generice  |  Detalii  |
| ------------- |---------- | ----- | ------------------ | --------- |
| ....    | string       | y     | required | detlaii |
