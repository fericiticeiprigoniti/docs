# Sumar
| Metoda | Endpoint     | Sumar                         |
| ------ | ---------    | ----------------------------- |
| GET    | [./biblioteca/carti](#get-bibliotecacarti)     | Lista cu toate cartile |
| GET    | [./biblioteca/autori](#get-bibliotecaautori)     | Lista cu autorii |
| GET    | [./biblioteca/categoriiCarti](#get-bibliotecacategoriicarti)     | Categoriile cartilor |


# GET ./biblioteca/carti
Lista cu toate cartile filtrare dupa anumite criterii

[Output schema](./get-bibliotecacarti-example.json) 
[Demo](http://fcp.cpco.ro/api/biblioteca/carti)

### GET params
| Nume          | Tip       | Req.  | Validari generice  |  Detalii  |
| ------------- |---------- | ----- | ------------------ | --------- |
| id    | int       | n     | numeric | ID-ul cartii |
| excludedId    | int       | n     |  | ID-ul cartii care vrem sa fie exclusa din lista |
| titlu    | string       | n     |  | Cauta in titlul cartii |
| subtitlu    | string       | n     |  | Cauta in subtitlu |
| searchTerm    | string       | n     |  | Cautare avansata in id & titlu & subtitlu & autor carte |
| categorieId    | int       | n     |  | ID-ul categoriei (vezi /biblioteca/categoriiCarti |
| edituraId    | int       | n     |  | ID-ul editurii |
| colectieId    | int       | n     |  | ID-ul colectiei (vezi /biblioteca/colectii) |
| parentId    | int       | n     |  | ID-ul cartii parinte |
| autorId    | int       | n     |  | ID-ul autorului |
| localitateId    | int       | n     |  | ID-ul localitatii |
| isPublished    | int       | n     |  | Daca este publicat 0/1|
| isDeleted    | int       | n     |  | Daca este dezactivata 0/1|

Alti parametri posibili pentru a fi folositi:

- @var int $versiune
- @var int $editia
- @var int $anulPublicatiei
- @var int $nrPaginiMin
- @var int $nrPaginiMax
- @var int $dimensiuneId
- @var int $format
- @var bool|null $areCoperta
- @var array $orderBy


# GET ./biblioteca/autori
Lista cu toti autorii cartilor

[Output schema](./get-bibliotecaautor-example.json) 
[Demo](http://fcp.cpco.ro/api/biblioteca/autori)

### GET params
| Nume          | Tip       | Req.  | Validari generice  |  Detalii  |
| ------------- |---------- | ----- | ------------------ | --------- |
| id    | int       | n     | numeric | ID-ul cartii |
| excludedId    | int       | n     |  | ID-ul cartii care vrem sa fie exclusa din lista |
| idList    | array       | n     |  | Lista cu id-urile autorilor. Pentru a face un singul call. |
| numeLike    | string       | n     |  | Cauta nume |
| prenumeLike    | string       | n     |  | Cauta prenume|
| aliasLike    | string       | n     |  | Cauta dupa alias |
| searchTerm    | string       | n     |  | Cautare avansata in id & nume & prenume & alias |
| isDeleted    | int       | n     |  | Daca este dezactivata 0/1|
| orderBy    | string       | n     |  | Ordonare (vezi variante mai jos) |

Order by:

- id_asc
- id_desc
- nume_asc
- nume_desc
- prenume_asc
- prenume_desc
